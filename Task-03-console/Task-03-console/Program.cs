﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03_console
{
    class Program
    {
        static void Main(string[] args)
        {            
            Console.WriteLine(q1);
            var txt = Console.ReadLine();

            Console.WriteLine(q2);
            var opt = int.Parse(Console.ReadLine());

            Console.WriteLine($"{choices(txt, opt)}");

            Console.WriteLine($" \nTo exit, press the \"Enter\" key.");
            Console.ReadLine();
        }

        static string q1 = $"Type text to be change:";
        static string q2 = $"What type of casing do you want to convert the text to? Select \"0\" for Uppercase, \"1\" for lowercase or \"2\" for change the first letter to Uppercase.";

        static string choices(string txt, int choice)
        {
            var answer = "";
            var txt1 = "";
            var txtsameUp = "";
            var txtsameDown = "";
            var txtsameFirst = "";
            txtsameUp = txt.ToUpper();              // txtsameXX is required for if-else statement to know when input and output are the same.
            txtsameDown = txt.ToLower();
            txtsameFirst = char.ToUpper(txt[0]) + txt.Substring(1);           
            txt1 = txt;


            switch (choice)
            {
                case 0:

                    if (txt == txtsameUp)
                    {
                        answer = $"{txt} is already in that form!";
                    }
                    else
                    {
                        answer = $"{txt} in Uppercase is {txt.ToUpper()}";
                    }

                    break;

                case 1:

                    if (txt == txtsameDown)
                    {
                        answer = $"{txt} is already in that form!";
                    }
                    else
                    {
                        answer = $"{txt} in lowercase is {txt.ToLower()} ";
                    }
                    break;

                case 2:
                    if (txt == txtsameUp) // if statement is used when user enters text in full captail leters to change to Title case.
                    {
                        txt = txt.ToLower(); // Makes full string lowercase
                        txt = char.ToUpper(txt[0]) + txt.Substring(1); // Now string is lowercase, we can now change first letter to captial
                        answer = $"{txt1} in title case is {txt} "; //prints correct user input, then change the output to the user
                    }
                    else if (txt == txtsameFirst)
                     {
                          txt = char.ToUpper(txt1[0]) + txt.Substring(1);
                          answer = $"{txt} is already in that form!";
                     }                         
                     else
                      {
                          txt = char.ToUpper(txt[0]) + txt.Substring(1);
                          answer = $"{txt1} in title case is {txt} ";
                      } 

                    break;

                default:
                    answer = $"Incorrect value given, Next time, please use \"0\" for Uppercase, \"1\" for Lowercase and \"2\" for Title.";
                    break;
            }

            return answer;
        }

        
    }
}
