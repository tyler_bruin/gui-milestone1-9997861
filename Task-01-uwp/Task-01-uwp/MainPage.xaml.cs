﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_01_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            lbl1.Text = q1;
            lbl2.Text = q2;
            lbl3.Text = "";

        }

        // ************************      ^^ User Interface ^^    **********************************************************
        // **************************************************************************************************************
        // ************************      vv Program Logic vv    **********************************************************

        static string q1 = $"What measurement do you wanting to convert:";
        static string q2 = $"What unit do you want to convert it to? Select \"0\" for Kilometers or \"1\" for Miles?";

        static string choices(string num, int choice)
        {
            var answer = "";

            switch (choice)
            {
                case 0:
                    answer = $"{num} Kilometers is equal to {kilometers2miles(double.Parse(num))} Miles";
                    break;
                case 1:
                    answer = $"{num} Miles is equal to {miles2kilometers(double.Parse(num))} Kilometers";
                    break;
                default:
                    answer = $"Incorrect value given, Next time, please use 0 for Kilometers and 1 for Miles";
                    break;
            }

            return answer;
        }

        static double miles2kilometers(double input)
        {
            const double converter = 1.609344;
            var mile = Convert.ToDouble(input);
            var result = mile * converter;

            result = System.Math.Round(result, 2);
            return result;
        }

        static double kilometers2miles(double input)
        {
            const double converter = 0.621371192;
            var km = Convert.ToDouble(input);
            var result = km * converter;

            result = System.Math.Round(result, 2);
            return result;
        }

        private void cb1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lbl3.Text = choices(tb1.Text, cb1.SelectedIndex);
        }
    }
}
