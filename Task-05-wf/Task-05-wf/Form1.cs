﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_05_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            lbl1.Text =(q0);
            lbl4.Text = (q1);
            //lbl3.Text =(q2);
           // lbl4.Text =(q3);
        }

        // ************************      ^^ User Interface ^^    **********************************************************
        // **************************************************************************************************************
        // ************************      vv Program Logic vv    **********************************************************

        static string q0 = "Welcome to the Guessing Game!";
        static string q1 = "\n\nRules:\nYou have to guess the correct number between One and Five, \nYou get a total of five guesses to get a high score of five.";
       // static string q2 = "Please enter your guess as a number, e.g. 1\n";
       // static string q3 = "To begin, Enter your first guess and press the \"Enter\" key.";

        static int score = 0;
        static int round = 0;
        static int roundsleft = 4;
        static int random = 0;


        static string checkguess(int guess)
        {
            var output = "";
            var ran = ranNumGen(random);

            if (ranNumGen(random) == guess)
            {

                score = score + 1;
                output = $"Correct! The correct answer is: {ranNumGen(random)} You guessed: {guess} You guessed the correct answer! \nYou have {roundsleft} rounds left. Currently your score is: {score}\n\nEnter your next guess";
            }
            else
            {
                output = $"Wrong! The correct answer is: {ranNumGen(random)} You guessed: {guess}. You guess the incorrect answer. \nYou have {roundsleft} turns left. Currently your score is: {score}\n\nEnter your next guess.";
            }

            round++;
            roundsleft--;

            return output;
        }

        static int ranNumGen(int random)
        {
            Random rnd = new Random();
            int result = rnd.Next(1, 6);

            return result;
        }

        private void bt1_Click(object sender, EventArgs e)
        { 
            if (round < 4)
            {
                lbl5.Text = checkguess(int.Parse(tb1.Text));
            }
            else
            {
                lbl3.Text = $"Game over!\nYour total score is: {score}";
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}