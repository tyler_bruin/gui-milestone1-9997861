﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            lbl1.Text = q1;
            lbl2.Text = q2;

        }

        // ************************      ^^ User Interface ^^    **********************************************************
        // **************************************************************************************************************
        // ************************      vv Program Logic vv    **********************************************************

        static string q1 = $"What measurement do you wanting to convert:";
        static string q2 = $"What unit do you want to convert it to? Select \"0\" for Kilometers or \"1\" for Miles?";

        static string choices(string num, int choice)
        {
            var answer = "";

            switch (choice)
            {
                case 0:
                    answer = $"{num} Kilometers is equal to {kilometers2miles(double.Parse(num))} Miles";
                    break;
                case 1:
                    answer = $"{num} Miles is equal to {miles2kilometers(double.Parse(num))} Kilometers";
                    break;
                default:
                    answer = $"Incorrect value given, Next time, please use 0 for Kilometers and 1 for Miles";
                    break;
            }

            return answer;
        }

        static double miles2kilometers(double input)
        {
            const double converter = 1.609344;
            var mile = Convert.ToDouble(input);
            var result = mile * converter;

            result = System.Math.Round(result, 2);
            return result;
        }

        static double kilometers2miles(double input)
        {
            const double converter = 0.621371192;
            var km = Convert.ToDouble(input);
            var result = km * converter;

            result = System.Math.Round(result, 2);
            return result;
        }

        private void cb1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbl3.Text = choices(tb1.Text, cb1.SelectedIndex);
        }
    }
}