﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_02_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            lbl1.Text = q1;
                       
        }

        // ************************      ^^ User Interface ^^    **********************************************************
        // **************************************************************************************************************
        // ************************      vv Program Logic vv    **********************************************************

        static string q1 = "Please enter the price of the item: ";              
        static double totalcost = 0;
        static double itemprice = 0;

        static string gst(double totalcost)
        {
 
            var output = "";
            totalcost = totalcost * 1.15;
            totalcost = System.Math.Round(totalcost, 2);
            output = Convert.ToString(totalcost);

            return output;
 
        }

        // Copied Case code into buttons as user input is no longer required to enter a option (e.g. 0 & 1), instead the user is required to press a button.

        private void button1_Click(object sender, EventArgs e)
        {
            
            itemprice = double.Parse(tb1.Text);            
            totalcost = itemprice + totalcost;

            lbl2.Text =$"Added item @ ${itemprice}";
            tb1.Text = String.Empty;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            var answer = "";           
            answer = $"The total cost of the shopping list including GST is ${gst(totalcost)}";
            
            lbl3.Text = answer;
            tb1.Text = String.Empty;

        }
    }
}
 