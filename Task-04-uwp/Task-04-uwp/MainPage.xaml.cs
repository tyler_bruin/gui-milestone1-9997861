﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_04_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            lbl1.Text = q1;
            lbl3.Text = q2;
        }

        // ************************      ^^ User Interface ^^    **********************************************************
        // **************************************************************************************************************
        // ************************      vv Program Logic vv    **********************************************************

        static string q1 = "Welcome to the Fruit and Vegetable dictionary, \nPlease type in the name of a fruit or vegetable to check if it is in our Dictionary.";
        static string q2 = "Tip: Want to search for another item? Type in the text box and press \"Check Dictionary\" again.";
        // Made changes to some strings between console and wf for things to make sense


        static string dictAnswer(string dictionary)
        {
            var inDict = "";
            dictionary = dictionary.ToLower(); // Required to accept any form of capitalization; 
            dictionary = char.ToUpper(dictionary[0]) + dictionary.Substring(1);

            var dictList = new Dictionary<string, string>();

            dictList.Add("Apple", "fruit");
            dictList.Add("Apples", "fruit");
            dictList.Add("Apricot", "fruit");
            dictList.Add("Apricots", "fruit");
            dictList.Add("Asparagus", "vegetable");
            dictList.Add("Avocado", "fruit");
            dictList.Add("Avocados", "fruit");
            dictList.Add("Banana", "fruit");
            dictList.Add("Bananas", "fruit");
            dictList.Add("Beetroot", "vegtable");
            dictList.Add("Beetroots", "vegtable");
            dictList.Add("Blackberry", "vegtable");
            dictList.Add("Blackberries", "vegtable");
            dictList.Add("Broccoli", "vegtable");
            dictList.Add("Cabbage", "vegtable");
            dictList.Add("Cabbages", "vegtable");
            dictList.Add("Carrot", "vegtable");
            dictList.Add("Carrots", "vegtable");
            dictList.Add("Celery", "vegtable");
            dictList.Add("Cherry", "fruit");
            dictList.Add("Cherries", "fruit");
            dictList.Add("Cucumber", "vegetable");
            dictList.Add("Cucumbers", "vegetable");
            dictList.Add("Date", "fruit");
            dictList.Add("Dates", "fruit");
            dictList.Add("Eggplant", "vegetable");
            dictList.Add("Eggplants", "vegetable");
            dictList.Add("Guava", "fruit");
            dictList.Add("Leek", "vegetable");
            dictList.Add("Leeks", "vegetable");
            dictList.Add("Lettuce", "vegetable");
            dictList.Add("Lettuces", "vegetable");
            dictList.Add("Lemon", "fruits");
            dictList.Add("Lemons", "fruits");
            dictList.Add("Melon", "fruit");
            dictList.Add("Melons", "fruit");
            dictList.Add("Pea", "vegetable");
            dictList.Add("Peas", "vegetable");
            dictList.Add("Pumpkin", "vegetable");
            dictList.Add("Pumpkins", "vegetable");
            dictList.Add("Potato", "vegetable");
            dictList.Add("Pineapple", "fruit");
            dictList.Add("Pineapples", "fruit");


            if (dictList.ContainsKey(dictionary))
            {
                inDict = $"{dictionary} is in our fruit and vegetable dictionary.";
                // changes to text to make sense in GUI application compaired to console.
            }
            else
            {
                inDict = $"{dictionary} is not in our fruit and vegetable dictionary.";
                // changes to text to make sense in GUI application compaired to console.
            }

            return inDict;
        }

        private void bt1_Click(object sender, RoutedEventArgs e)
        {
            lbl2.Text = dictAnswer(tb1.Text);
        }
    }
}
