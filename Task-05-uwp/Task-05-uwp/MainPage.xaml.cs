﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_05_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            lbl1.Text = (q0);
            lbl4.Text = (q1);
        }
        // ************************      ^^ User Interface ^^    **********************************************************
        // **************************************************************************************************************
        // ************************      vv Program Logic vv    **********************************************************

        static string q0 = "Welcome to the Guessing Game!";
        static string q1 = "\n\nRules:\nYou have to guess the correct number between One and Five, \nYou get a total of five guesses to get a high score of five.";
       // static string q2 = "Please enter your guess as a number, e.g. 1\n";
       // static string q3 = "To begin, Enter your first guess and press the \"Enter\" key.";

        static int score = 0;
        static int round = 0;
        static int roundsleft = 4;
        static int random = 0;


        static string checkguess(int guess)
        {
            var output = "";
            var ran = ranNumGen(random);

            if (ranNumGen(random) == guess)
            {
                score = score + 1;
                output = $"Correct! The correct answer is: {ranNumGen(random)} You guessed: {guess} You guessed the correct answer! You have {roundsleft} rounds left. Currently your score is: {score}\nEnter your next guess";
            }
            else
            {
                output = $"Wrong! The correct answer is: {ranNumGen(random)} You guessed: {guess}. You guess the incorrect answer. You have {roundsleft} turns left. Currently your score is: {score}\nEnter your next guess.";
            }

            round++;
            roundsleft--;

            return output;
        }

        static int ranNumGen(int random)
        {
            Random rnd = new Random();
            int result = rnd.Next(1, 6);

            return result;
        }

        private void bt1_Click(object sender, RoutedEventArgs e)
        {
            if (round < 4)
            {
                lbl5.Text = checkguess(int.Parse(tb1.Text));
            }
            else
            {
                lbl3.Text = $"Game over!\n Your total score is: {score}";
            }
        }
    }
}