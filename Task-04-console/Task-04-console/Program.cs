﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_console
{
    class Program
    {
        static int welcome = 0;

        static void Main(string[] args)
        {
            
            while(true)
            {
                int opt;

                if (welcome < 1)
                {
                    Console.WriteLine(q0);
                     welcome++;
                 }

                Console.WriteLine(q1);
                var dictionary = Console.ReadLine();

                Console.WriteLine(dictAnswer(dictionary));
                Console.ReadLine();

                Console.WriteLine(q2);
                Console.WriteLine($"Then press the \"Enter\" key to continue");
                opt = int.Parse(Console.ReadLine());

                if (opt == 0)
                {
                    
                }
                else
                {
                    Console.WriteLine($"To exit, press the \"Enter\" key.");
                    Console.ReadLine();
                    break;
                }
            }           
        }

        // ************************      ^^ User Interface ^^    **********************************************************
        // **************************************************************************************************************
        // ************************      vv Program Logic vv    **********************************************************

        static string q0 = ("Welcome to the Fruit and Vegetable dictionary!"); 
        static string q1 = ("\nPlease type in the name of a fruit or vegetable to check if it is in our Dictionary.");
        static string q2 = "Search for another item?  If Yes, select \"0\", if No type \"1\" ";
        

        static string dictAnswer(string dictionary)
        {
            var inDict = "";
            dictionary = dictionary.ToLower(); // Required to accept any form of capitalization; 
            dictionary = char.ToUpper(dictionary[0]) + dictionary.Substring(1);

            var dictList = new Dictionary<string, string>();

            dictList.Add("Apple", "fruit");                 
            dictList.Add("Apples", "fruit");
            dictList.Add("Apricot", "fruit");
            dictList.Add("Apricots", "fruit");
            dictList.Add("Asparagus", "vegetable");
            dictList.Add("Avocado", "fruit");
            dictList.Add("Avocados", "fruit");
            dictList.Add("Banana", "fruit");
            dictList.Add("Bananas", "fruit");
            dictList.Add("Beetroot", "vegtable");
            dictList.Add("Beetroots", "vegtable");
            dictList.Add("Blackberry", "vegtable");
            dictList.Add("Blackberries", "vegtable");
            dictList.Add("Broccoli", "vegtable");
            dictList.Add("Cabbage", "vegtable");
            dictList.Add("Cabbages", "vegtable");
            dictList.Add("Carrot", "vegtable");
            dictList.Add("Carrots", "vegtable");
            dictList.Add("Celery", "vegtable");
            dictList.Add("Cherry", "fruit");
            dictList.Add("Cherries", "fruit");
            dictList.Add("Cucumber", "vegetable");
            dictList.Add("Cucumbers", "vegetable");
            dictList.Add("Date", "fruit");
            dictList.Add("Dates", "fruit");
            dictList.Add("Eggplant", "vegetable");
            dictList.Add("Eggplants", "vegetable");
            dictList.Add("Guava", "fruit");
            dictList.Add("Leek", "vegetable");
            dictList.Add("Leeks", "vegetable");
            dictList.Add("Lettuce", "vegetable");
            dictList.Add("Lettuces", "vegetable");
            dictList.Add("Lemon", "fruits");
            dictList.Add("Lemons", "fruits");
            dictList.Add("Melon", "fruit");
            dictList.Add("Melons", "fruit");
            dictList.Add("Pea", "vegetable");
            dictList.Add("Peas", "vegetable");
            dictList.Add("Pumpkin", "vegetable");
            dictList.Add("Pumpkins", "vegetable");
            dictList.Add("Potato", "vegetable");
            dictList.Add("Pineapple", "fruit");
            dictList.Add("Pineapples", "fruit");

            

            if (dictList.ContainsKey(dictionary))
            {
                inDict = $"{dictionary} is in our fruit and vegetable dictionary. \nPress the \"Enter\" key to continue ";
                
            }
            else
            {
                inDict = $"{dictionary} is not in our fruit and vegetable dictionary. \npress the \"Enter\" key to continue";
               
            }

            return inDict;
        }
    }
}
