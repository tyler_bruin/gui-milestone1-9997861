﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05_console
{
    class Program
    {
        static void Main(string[] args)
        {
            var guess = 0;

            Console.WriteLine(q0);
            Console.WriteLine(q1);
            Console.WriteLine(q2);
            Console.WriteLine(q3);

            while (round < 5)
            {

                guess = int.Parse(Console.ReadLine());
                Console.WriteLine(checkguess(guess));
            }
           
          //  Console.Clear();
            Console.WriteLine($"\nYour total score is: {score}"); 
            Console.WriteLine($"\nTo exit, press the \"Enter\" key.");
            Console.ReadLine();

        }

        // ************************      ^^ User Interface ^^    **********************************************************
        // **************************************************************************************************************
        // ************************      vv Program Logic vv    **********************************************************

        static string q0 = "Welcome to the Guessing Game!";
        static string q1 = "\nRules:\nYou have to guess the correct number between One and Five, \nYou get a total of five guesses to get a high score of five.";
        static string q2 = "Please enter your guess as a number, e.g. 1\n";
        static string q3 = "To begin, Enter your first guess and press the \"Enter\" key.";
      
        static int score = 0;
        static int round = 0;
        static int roundsleft = 4;
        static int random = 0;


        static string checkguess(int guess)
        {
            var output = "";
            var ran = ranNumGen(random);

            if (ranNumGen(random) == guess)
            {
                Console.Clear();
                score = score + 1;
                output = $"Correct! The correct answer is: {ranNumGen(random)} You guessed: {guess} You guessed the correct answer! You have {roundsleft} rounds left. Currently your score is: {score}\n\nEnter your next guess";
            }
            else
            {
                Console.Clear();
                output = $"Wrong! The correct answer is: {ranNumGen(random)} You guessed: {guess}. You guess the incorrect answer. You have {roundsleft} turns left. Currently your score is: {score}\n\nEnter your next guess.";
            }

            round++;
            roundsleft--;

            return output;
        }

        static int ranNumGen(int random)
        {
            Random rnd = new Random();
            int result = rnd.Next(1, 6);

            return result;
        }
    }
}
