﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_02_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            lbl1.Text = q1;
        }
        // ************************      ^^ User Interface ^^    **********************************************************
        // **************************************************************************************************************
        // ************************      vv Program Logic vv    **********************************************************

        static string q1 = "Please enter the price of the item: ";
        static double totalcost = 0;
        static double itemprice = 0;

        static string gst(double totalcost)
        {

            var output = "";
            totalcost = totalcost * 1.15;
            totalcost = System.Math.Round(totalcost, 2);
            output = Convert.ToString(totalcost);

            return output;

        }
        // Copied Case code into buttons as user input is no longer required to enter a option (e.g. 0 & 1), instead the user is required to press a button.

        private void bt1_Click(object sender, RoutedEventArgs e)
        {
            itemprice = double.Parse(tb1.Text);
            totalcost = itemprice + totalcost;

            lbl2.Text = $"Added item @ ${itemprice}";
            tb1.Text = String.Empty;
        }

        private void bt2_Click(object sender, RoutedEventArgs e)
        {
            var answer = "";
            answer = $"The total cost of the shopping list including GST is ${gst(totalcost)}";

            lbl3.Text = answer;
            tb1.Text = String.Empty;
        }
    }
}
